package com.company;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        try {
            Document doc = Jsoup.connect("https://play.google.com/store/apps/category/FINANCE/collection/topselling_paid").timeout(0).get();
            Document detailDoc = null;

            HashSet<String> hrefs = new HashSet<String>();

            Elements anchors = doc.getElementsByClass("card-click-target");
            for (Element element : anchors) {
                String url = "https://play.google.com/" + element.attr("href").toString();
                hrefs.add(url);
            }

            for (String url : hrefs) {
                detailDoc = Jsoup.connect(url).timeout(0).get();
                String nombre = detailDoc.select("[class='id-app-title']").text();
                String description = detailDoc.select("[itemprop='description']").text();
                String numero_de_ratings = detailDoc.select("[class='rating-count']").text();
                String score = detailDoc.select("[class='score']").text();
                String recent_change = detailDoc.select("[class='recent-change']").text();
                String rating_bar_container_five = detailDoc.select("[class='rating-bar-container five']").text();
                String rating_bar_container_four = detailDoc.select("[class='rating-bar-container four']").text();

                System.out.println("Nombre: " + nombre);
                System.out.println("Número de ratings: " + numero_de_ratings);
                System.out.println("Rating promedio: " + score);
                System.out.println("Descripción: " + description);
                System.out.println("Cambios recientes: " + recent_change);
                System.out.println("Ratings con 5 estrellas: " + rating_bar_container_five);
                System.out.println("Ratings con 4 estrellas: " + rating_bar_container_four);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
